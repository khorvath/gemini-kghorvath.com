```
    __ __                _          __  __                       __  __  
   / //_/___ _____ ___  (_)___     / / / /___  ______   ______ _/ /_/ /_ 
  / ,< / __ `/ __ `__ \/ / __ \   / /_/ / __ \/ ___/ | / / __ `/ __/ __ \
 / /| / /_/ / / / / / / / / / /  / __  / /_/ / /   | |/ / /_/ / /_/ / / /
/_/ |_\__,_/_/ /_/ /_/_/_/ /_/  /_/ /_/\____/_/    |___/\__,_/\__/_/ /_/ 
```                                                                         

Hi, my name is Kamin.

By day I’m a Mechanical Engineer in Atlanta, GA. By night I’m a tech enthusiast with a passion for free and open source software. I love to tinker with computers and I have a number of personal projects I’m proud to show off above.

In my free time, I enjoy gaming, home improvement, and being a dad.

## Latest Posts

=>posts/gemini.gmi I'm now in Geminispace
=>posts/getting-started-microvms.gmi Getting Started with MicroVMs
=>posts/libvirt-in-podman.gmi Libvirt in Podman

## Links

=>posts.gmi Posts
=>resume.gmi Resume
=>contact.gmi Contact
